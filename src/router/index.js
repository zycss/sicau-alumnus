import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  component: () => import('../views/Home'),
  children: [{
      path: "/",
      redirect: "/home"
    },
    {
      path: 'home',
      component: () => import('../views/Home/home.vue'),
      name: "四川农业大学校友会"
    },
    {
      name: "各院校联系方式",
      path: "connectTo",
      component: () => import('views/Home/connect')
    },
    {
      name: "校友服务",
      path: "alumni/linkage",
      component: () => import('views/Home/service/index.vue')
    },
    {
      name:"宣传资料",
      path:"alumni/publicity",
      component:()=>import('views/Home/service/publicity')
    },
    {
      name: "学校地图",
      path: "alumni/map",
      component: () => import('views/Home/service/map/index.vue'),
      children:[
        {
          path:"",
          redirect:"chengdu"
        },
        {
          name:"学校地图-成都校区",
          path:"chengdu",
          component:()=>import('views/Home/service/map/chengdu.vue')
        },
        {
          name:"学校地图-雅安校区",
          path:"yaan",
          component:()=>import('views/Home/service/map/yaan.vue')
        },
        {
          name:"学校地图-都江堰校区",
          path:"dujiangyan",
          component:()=>import('views/Home/service/map/dujiangyan.vue')
        },
      ]
    },
    {
      name: "周边交通",
      path: "alumni/traffic",
      component: () => import('views/Home/service/traffic/index.vue'),
      children:[
        {
          path:"",
          redirect:"chengdu"
        },
        {
          name:"周边交通-成都校区",
          path:"chengdu",
          component:()=>import('views/Home/service/traffic/chengdu.vue')
        },
        {
          name:"周边交通-雅安校区",
          path:"yaan",
          component:()=>import('views/Home/service/traffic/yaan.vue')
        },
        {
          name:"周边交通-都江堰校区",
          path:"dujiangyan",
          component:()=>import('views/Home/service/traffic/dujiangyan.vue')
        },
      ]
    },
    {
      name: "周边食宿信息",
      path: "alumni/food",
      component: () => import('views/Home/service/food/index.vue'),
      children:[
        {
          path:"",
          redirect:"chengdu"
        },
        {
          name:"周边食宿信息-成都校区",
          path:"chengdu",
          component:()=>import('views/Home/service/food/chengdu.vue')
        },
        {
          name:"周边食宿信息-雅安校区",
          path:"yaan",
          component:()=>import('views/Home/service/food/yaan.vue')
        },
        {
          name:"周边食宿信息-都江堰校区",
          path:"dujiangyan",
          component:()=>import('views/Home/service/food/dujiangyan.vue')
        },
      ]
    },
    {
      path: "register",
      component: () => import('../views/Home/register'),
      children: [{
        path: "",
        name: "校友登录",
        component: () => import('views/Home/regForm.vue')
      }, {
        path: "completed",
        component: () => import('views/Home/regCompleted.vue')
      }]
    },
    {
      path: "/alumni/connect",
      component: () => import('../views/Alumni/connect/index.vue'),
      children: [{
          name: "各地校友会",
          path: "place",
          component: () => import('../views/Alumni/connect/place.vue')
        },
        {
          name: "院所校友工作",
          path: "work",
          component: () => import('../views/Alumni/connect/work.vue')
        }
      ]
    },
    {
      path: "/alumni/us",
      component: () => import('../views/Alumni/us/index.vue'),
      children: [{
          name: "校友会介绍",
          path: "alumni",
          component: () => import('../views/Alumni/us/alumni.vue')
        },
        {
          name: "组织机构介绍",
          path: "organization",
          component: () => import('../views/Alumni/us/organization.vue')
        }
      ]
    },
    {
      path: "/fund/donate",
      component: () => import('../views/fund/donate/index.vue'),
      children: [{
          name: "筹款项目",
          path: "project",
          component: () => import('../views/fund/donate/project.vue')
        },
        {
          name: "项目详情",
          path: "project/:id",
          component: () => import('views/fund/donate/project-detail.vue')
        },
        {
          name: "在线捐赠",
          path: "online",
          component: () => import('../views/fund/donate/online.vue')
        },
        {
          name: "捐款流程",
          path: "process",
          redirect: "process/donate",
          component: () => import('../views/fund/donate/process/index.vue'),
          children: [{
              path: "donate",
              component: () => import('../views/fund/donate/process/process.vue')
            },
            {
              path: "ensure",
              component: () => import('../views/fund/donate/process/ensure')
            }
          ]
        },
        {
          name: "捐款公示",
          path: "show",
          component: () => import('../views/fund/donate/show.vue')
        },
      ]
    },
    {
      path: "/fund/info",
      component: () => import('../views/fund/info/index.vue'),
      children: [{
          name: "年度报告详情",
          path: "annual/:id",
          component: () => import('../views/fund/info/annual.vue')
        },
        {
          name: "年度报告",
          path: "annualList",
          component: () => import('../views/fund/info/annuallist.vue')
        },
        {
          name: "捐赠名单",
          path: "donatelist",
          component: () => import('../views/fund/info/donatelist.vue')
        },
        {
          name: "捐赠详情",
          path: "donatelist/:id",
          component: () => import('../views/fund/info/donateDetail.vue')
        },
        {
          name: "受益方公示",
          path: "benefit",
          component: () => import('../views/fund/info/benefit.vue')
        },
        {
          path: "benefit/:id",
          component: () => import('../views/fund/info/benefitDetail.vue')
        },
        {
          name: "其他公示",
          path: "other",
          component: () => import('../views/fund/info/other.vue')
        },
        {
          path: "other/:id",
          component: () => import('../views/fund/info/otherDetail.vue')
        }
      ]
    },
    {
      path: "/fund/us",
      component: () => import('../views/fund/us/index.vue'),
      children: [{
          name: "基金会简介",
          path: "intro",
          component: () => import('../views/fund/us/intro.vue')
        },
        {
          name: "组织机构",
          path: "organization",
          component: () => import('../views/fund/us/organization.vue')
        },
        {
          name: "章程",
          path: "rule",
          component: () => import('../views/fund/us/rule.vue')
        },
      ]
    },
    {
      //联系我们
      path: "connect",
      component: () => import('../views/connect/index.vue'),
      children: [{
          name: "",
          path: "/",
          redirect: "basic"
        },
        {
          name: "联系方式",
          path: "basic",
          component: () => import('../views/connect/basic.vue')
        },
        {
          name: "联系方式-校友会",
          path: "alumni",
          component: () => import('../views/connect/alumni.vue')
        },
        {
          name: "联系方式-基金会",
          path: "fund",
          component: () => import('../views/connect/fund.vue')
        }
      ]
    },
    {
      path: "rule",
      component: () => import('../views/rule/index.vue'),
      children: [{
          name: "免税资格",
          path: "tax",
          component: () => import('../views/rule/tax.vue')
        },
        {
          name: "票据样式",
          path: "bill",
          component: () => import('../views/rule/bill.vue')
        },
        {
          name: "管理制度",
          path: "manager",
          component: () => import('../views/rule/manager.vue')
        }, {
          name: "法规政策",
          path: "policy",
          component: () => import('../views/rule/policy.vue')
        },
        {
          name: "",
          path: "policy/:id",
          component: () => import('views/rule/policy-detail.vue')
        },
        {
          name: "",
          path: "manager/:id",
          component: () => import('../views/rule/managerInfo.vue')
        }
      ]
    },
    {
      name: "最新捐款",
      path: "donate",
      component: () => import('../views/donate')
    },
    {
      path: "news",
      component: () => import('../views/news/more'),
      redirect: "/news/list",
      children: [{
        name: "更多新闻",
        path: "list",
        component: () => import('../views/news/more/newsList.vue')
      }, {
        name: "",
        path: "info/:id",
        component: () => import('../views/news/more/newDetail.vue')
      }]
    },
    {
      path: "glory",
      component: () => import('../views/glory'),
      redirect: "/glory/list",
      children: [{
          name: "校友风采",
          path: "list",
          component: () => import('../views/glory/gloryList.vue')
        },
        {
          path: "info/:id",
          component: () => import('../views/glory/glorydetail.vue')
        }
      ]
    },
    {
      path: "p-news",
      component: () => import('../views/news/pic'),
      redirect: '/p-news/list',
      children: [{
          name: "更多新闻",
          path: "list",
          component: () => import('../views/news/pic/list.vue')
        },
        {
          name: "",
          path: "info/:id",
          component: () => import('../views/news/pic/detail.vue')
        }
      ]
    }
  ]
}]

const router = new VueRouter({
  routes
})

router.afterEach((to, from, next) => {
  window.scrollTo(0, 0)
});
export default router