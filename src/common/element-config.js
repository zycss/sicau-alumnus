import {
  Button,
  Select,
  Carousel,
  CarouselItem,
  Link,
  Input,
  Table,
  TableColumn,
  Radio,
  RadioGroup,
  Option,
  Breadcrumb,
  BreadcrumbItem,
  Pagination,
  DatePicker,
} from 'element-ui'

export const install = (Vue) => {
  Vue.use(Button)
  Vue.use(Select)
  Vue.use(Carousel)
  Vue.use(CarouselItem)
  Vue.use(Link)
  Vue.use(Input)
  Vue.use(Table)
  Vue.use(Radio)
  Vue.use(RadioGroup);
  Vue.use(TableColumn)
  Vue.use(Option)
  Vue.use(Breadcrumb)
  Vue.use(BreadcrumbItem)
  Vue.use(Pagination)
  Vue.use(DatePicker)


}