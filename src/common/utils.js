// 一维数组 -> 二维数组,分页功能
export const one2arr = (curryArr, pageSize) => {
  let j = 0;
  let data = [];
  for (let i = 0; i < curryArr.length; i++) {
    if (i % pageSize == 0) {
      j++;
      data[j - 1] = new Array();
    }
    data[j - 1][i % pageSize] = curryArr[i];
  }
  return data
}
