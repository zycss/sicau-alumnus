import axios from 'axios'
import qs from 'qs'
const isDev = process.env.NODE_ENV === 'development'
const service = axios.create({
  baseURL: isDev ? "http://39.101.167.48:8080/alumfound" : "http://39.101.167.48:8080/alumfound"
})
service.interceptors.request.use((config) => {
  return config
})
service.interceptors.response.use((res) => {
  if(res.config.url == "/alumniinfo"){
    return res.data
  }
  return res.data.data
})
//----校友风采----
export const alumnimienList = (params) => {
  return service.get('/alumnimien', {
    params
  }) //校友风采列表
}
export const alumnimienInfo = (id) => {
  return service.get(`/alumnimien/${id}`) //校友风采详情
}
//----数据统计----
export const statisticsPost = (date) => {
  return service.post(`/statistics/visit/${date}`) //新增数据
}
export const statisticsGet = (date) => {
  return service.get(`/statistics/info/${date}`) //查看数据
}
//----普通新闻----
export const getAllNews = (params) => {
  return service.get('/news', {
    params
  })//获取全部新闻
}
export const getAlumniNews = (params) => {
  return service.get('/news/alumni', {
    params
  }) //获取校友会新闻
}
export const getFundNews = (params) => {
  return service.get('/news/foundation', {
    params
  }) //获取基金会新闻
}
export const getNewsInfo = (id) => {
  return service.get('/news/' + id) //获取单个新闻详情
}
//----图片新闻----
export const picNews = (params) => {
  return service.get('/picnews', {
    params: params
  }) //图片新闻列表
}
export const picNewsInfo = (id) => {
  return service.get(`/picnews/${id}`) //图片新闻详情
}

//----校友登记----
export const alumniinfo = (obj) => {
  return service.post('/alumniinfo',obj)
}
//----轮播图----
export const banner = () => {
  return service.get('/banner') //轮播图
}
//----捐赠信息----
export const donateInfo = (params) => {
  return service.get('/donationinfo', {
    params
  }) //捐赠信息列表
}

// ----规章制度----
export const manageSystem = (currentPage, size) => {
  return service.get('/managesystem', {
    params: {
      currentPage,
      size
    }
  }) // 管理制度列表
}

export const manageItem = (id) => {
  return service.get(`/managesystem/${id}`) // 管理制度信息
}

export const lawregulation = (currentPage, size) => {
  return service.get(`/lawregulation`, {
    params: {
      currentPage,
      size
    }
  }) // 政策法规列表
}

export const lawItem = (id) => {
  return service.get(`/lawregulation/${id}`) // 政策法规信息
}

// ----信息公开----
export const donationlist = (currentPage, size) => {
  return service.get(`/donationlist`, {
    params: {
      currentPage,
      size
    }
  }) // 捐赠信息列表
}

export const donationItem = (id) => {
  return service.get(`/donationlist/${id}`) // 捐赠信息
}

export const yearlyreport = (currentPage, size) => {
  return service.get(`/yearlyreport`, {
    params: {
      currentPage,
      size
    }
  }) // 年度报告
}

export const yearlyreportItem = (id) => { // 年度报告信息
  return service.get(`/yearlyreport/${id}`)
}

export const elsepubinfo = (currentPage, size) => { // 其他公示
  return service.get(`/elsepubinfo`, {
    params: {
      currentPage,
      size
    }
  })
}

export const elsepubinfoItem = (id) => { // 查询其他公示
  return service.get(`/elsepubinfo/${id}`)
}

export const beneficiary = (currentPage, size) => { // 受益人公示
  return service.get(`/beneficiary`, {
    params: {
      currentPage,
      size
    }
  })
}

export const beneficiaryItem = (id) => { // 查询受益人信息
  return service.get(`/beneficiary/${id}`)
}

// ----友情捐赠----
export const  donationProject = ()=>{ // 查询受益人信息
  return service.get(`/donationproject`)
}

export const  donationinfo = (currentPage,size)=>{ // 捐赠信息
  return service.get(`/donationinfo`,{
    params:{
      currentPage,
      size
    }
  })
}