import Vue from 'vue';
import 'normalize.css/normalize.css'
import './style/theme/index.css';
import App from './App.vue';
import 'babel-polyfill'
import router from './router/index.js'
import {install} from './common/element-config'
install(Vue)
new Vue({
  el: '#app',
  router,
  render: h => h(App)
});